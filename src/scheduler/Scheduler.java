package scheduler;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
    @author Samuel Allen
**/

public class Scheduler extends Application
{
	/**
        Starts application:
        
        @param  stage       :   Stage, the main Stage.
        @throws Exception
    **/
    
	@Override
	
	public void start(Stage stage) throws Exception
	{
        // Declare variables:
        
		FXMLLoader loader;
		Parent root;
        SchedulerFXMLController controller;
        Scene scene;
        
        // Initialize FXMLLoader, Parent and controller:
        
        loader = new FXMLLoader(getClass().getResource("Scheduler.fxml"));
        root = loader.load();
        
		controller = loader.getController();
		controller.setParameters(getParameters());
		
        // Create new Scene:
        
	    scene = new Scene(root);
		
        // Initialize stage and show it:
        
		stage.setResizable(false);
		stage.setScene(scene);
		stage.show();
	}

	/**
        Main method:
        
	    @param  args    :   Command line arguments.
	**/
	
	public static void main(String[] args)
	{
		launch(args);
	}
	
}
