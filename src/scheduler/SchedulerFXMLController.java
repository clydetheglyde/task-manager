package scheduler;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;

/**
	@author Samuel Allen
**/

public class SchedulerFXMLController implements Initializable
{
	// Class fields:
	
	// FXML components:
	
	@FXML
	
	private Pane itemListPane;
	
	// Other UI elements:
	
	private ListView itemList;
	
	// Stores application data:
	
	private Map<String, Boolean> cellMap;
	private ObservableList<String> names;
	
	// For file IO:
	
	private Application.Parameters parameters;
	private FileManager file;
	
    /*
        Custom list cell for ListView:
    */
    
	private final class SchedulerCell extends ListCell<String>
	{
        // Class constants:
        
        // Holds content:
        
		private final HBox container;
        
        // Used to display the name of the list cell:
        
		private final Label label;
        
        // Used to space out HBox items:
        
		private final Pane labelButtonSeperator;
		private final Pane buttonEdgeSeperator;
        
        // Button oriented objects:
        
		private final Button button;
		private final EventHandler<ActionEvent> buttonPressed;
        
        // Controls button actions:
        
		private final Map<Boolean, ButtonOption> buttonOptionMap;
		
        // Default constructor:
        
		public SchedulerCell()
		{
            // Initialize parent:
            
			super();
			
            // Initialize HBox and its children:
            
			container = new HBox();
			label = new Label();
			labelButtonSeperator = new Pane();
			buttonEdgeSeperator = new Pane();
			button = new Button();
			
            // Initialize map to hold each button action:
            
			buttonOptionMap = new HashMap<>();
            
			buttonOptionMap.put(true, new PressButton());
			buttonOptionMap.put(false, new ReleaseButton());
			
            // Create new EventHandler for HBox button:
            
			buttonPressed = (ActionEvent event) ->
			{
                flipButtonState();
			};
			
            /*
                Tell the button to use the buttonPressed EventHandler and
                initialize its format:
            */
            
			button.setOnAction(buttonPressed);
			button.setText("  ");
			button.setStyle("-fx-background-radius: 40;");
			
            // Tell the HBox to hold all the content and initialize its style:
            
			container.setPrefWidth(0);
			container.setPadding(new Insets(40, 0, 0, 0));
            
			container.getChildren().addAll(label, labelButtonSeperator, button,
                                           buttonEdgeSeperator);
			HBox.setHgrow(labelButtonSeperator, Priority.ALWAYS);
			
            // Initialize label content and style:
            
			label.setWrapText(true);
			label.setPadding(new Insets(0, 0, 0, 40));
			label.setId("cellLabel");
            
            // Format each seperator pane:
            
			labelButtonSeperator.setPadding(new Insets(60, 0, 60, 40));
			buttonEdgeSeperator.setPadding(new Insets(0, 0, 0, 40));
		}
        
        /*
            Used to flip the state of the button:
        */
        
        private void flipButtonState()
        {
            // Change the state and update it:
            
            cellMap.put(label.getText(), !cellMap.get(label.getText()));
            buttonOptionMap.get(cellMap.get(label.getText())).doOption();
            
            // Write the change to a file:
            
			try
			{
				file.write(cellMap);
			}
			catch (IOException exception)
			{
				System.out.println(exception.getMessage());
			}
        }
		
        /*
            Abstract class for handling button actions:
        */
        
		private abstract class ButtonOption
		{
            /*
                Used to handle specific button action:
            */
            
			public abstract void doOption();
		}
		
        /*
            Used to activate button:
        */
        
		private class PressButton extends ButtonOption
		{
            /*
                Changes button style to represent button activation:
            */
            
			@Override
			
			public void doOption()
			{
				button.setId("listCellButtonPressed");
			}
		}
		
        /*
            Used to deactivate button:
        */
        
		private class ReleaseButton extends ButtonOption
		{
            /*
                Changes button style to represent button deactivation:
            */
            
			@Override
			
			public void doOption()
			{
				button.setId("listCellButton");
			}
		}
		
        /**
            Updates list cell:
            
            @param  name    :   String.
            @param  empty   :   boolean.
        **/
        
		@Override
		
		public void updateItem(String name, boolean empty)
		{
            // Call parent method:
            
			super.updateItem(name, empty);
			
            // If blank:
            
			if (name == null || empty)
			{
                // Reset styling:
                
				buttonOptionMap.get(false).doOption();
                
				label.setText(null);
				setGraphic(null);
			}
			else
			{
                // Update Styling:
                
				buttonOptionMap.get(cellMap.get(name)).doOption();
                
				label.setText(name);
				setGraphic(container);
			}
		}
	}
	
	/**
		Called to assign command line parameters to this FXML controller:
		
		@param	inParameters	:	Application.Parameters, the parameters to
									give.
	**/
	
	public void setParameters(Application.Parameters inParameters)
	{
		// Declare variables:
		
		int listSize;
		List<String> parameterList;
		Map<Integer, ParameterOption> parameterOptions;
		
		// Initialize parameter actions:
		
		parameterOptions = new HashMap<>();
		
		parameterOptions.put(1, new DirectoryOnly());
		
		// Initialize parameters and validate them:
		
		parameters = inParameters;
		
		if (parameters != null)
		{
			// Get a list of the given parameters, and validate the list:
			
			parameterList = parameters.getRaw();
			listSize = parameterList.size();
			
			if (parameterOptions.containsKey(listSize))
			{
				// If the parameters given were valid:
				
				parameterOptions.get(listSize).doOption(parameterList);
			}
		}
	}
	
	/*
		Abstract class for handling parameters:
	*/
	
	private abstract class ParameterOption
	{
        // Class constants:
        
        protected String invalidParameterMessage = ("Amount of Parameters " +
                                                    "Given was Invalid.");
        
        // Class fields:
        
        private Integer requiredParameterAmount;
        
        // Default constructor:
        
        public ParameterOption() throws IllegalArgumentException
        {
			setRequiredParameterAmount(0);
        }
        
        // Alternate constructor:
        
        public ParameterOption(int inRequiredParameterAmount)
                               throws IllegalArgumentException
        {
            try
            {
                setRequiredParameterAmount(inRequiredParameterAmount);
            }
            catch (IllegalArgumentException exception)
            {
                throw (exception);
            }
        }
        
        /**
            Sets the required parameter amount to the given value:
            
            @param  inRequiredParameterAmount   :   The given value.
            @throws IllegalArgumentException
        **/
        
        private void setRequiredParameterAmount(int inRequiredParameterAmount)
                                                throws IllegalArgumentException
        {
            // Check if the given value is valid and initialize it:
            
            if (inRequiredParameterAmount >= 0)
            {
                requiredParameterAmount = inRequiredParameterAmount;
            }
            else
            {
                // If the value was invalid:
                
                throw new IllegalArgumentException("There Cannot be Less " +
                                                   "Than Zero Parameters.");
            }
        }
        
        // Abstract methods:
        
        /**
            Used to call the given method for each parameter action:
            
            @param  parameterList   :   List, the list of parameters needed to
                                        perform the required processes with.
        **/
        
		public abstract void doOption(List<String> parameterList);
        
        // Instance methods:
        
        /**
            Validates that the given amount of parameters was correct, and
            returns a message for this validation:
            
            @param  inList  :   List, the list of parameters to check.
            @return String  :   Returns parameter integrity message.
        **/
        
        protected String validateParameters(List<String> inList)
        {
            // Declare variables:
            
            Boolean condition;
            Map<Boolean, String> parameterIntegrityMap;
            
            // Initialize messages for parameter integrity:
            
            parameterIntegrityMap = new HashMap<>();
            
            parameterIntegrityMap.put(true, null);
            parameterIntegrityMap.put(false, invalidParameterMessage);
            
            // Validate parameters and return outcome:
            
            condition = validateParameterLogic(inList);
            
            return (parameterIntegrityMap.get(condition));
        }
        
        /**
            Validates that the given amount of parameters was correct. Can be
            overridden for more complicated validation:
            
            @param  inList  :   List, the list of parameters to check.
            @return Boolean :   Returns true if the given parameters were
                                valid.
        **/
        
        protected Boolean validateParameterLogic(List<String> inList)
        {
            return (inList != null &&
                    inList.size() == requiredParameterAmount);
        }
	}
	
	/*
		Handles when only the directory is given as a commandline argument:
	*/
	
	private class DirectoryOnly extends ParameterOption
	{
        // Default constructor:
        
        public DirectoryOnly()
        {
            super (1);
        }
        
        /**
            Handles reading the given directory, expects one parameter:
            
            @param  parameterList   :   List, the list containing only the
                                        directory parameter.
        **/
        
		@Override
		
		public void doOption(List<String> parameterList)
		{
            // Declare variables:
            
            String validationMessage;
            
            // Validate parameters:
            
            validationMessage = validateParameters(parameterList);
            
            // If the parameters were valid:
            
            if (validationMessage == null)
            {
                // Try to read the given file:
                
                try
                {
                    // Create a new FileManager with the found file name:
                    
                    file = new FileManager(parameterList.get(0));
                    
                    /*
                        Read the file and create an observable ArrayList of
                        names:
                    */
                    
                    cellMap = file.read();
                    names = FXCollections.observableArrayList(cellMap.keySet());
                    
                    // Initialize a new ListView with the found names:
                    
                    itemList = new ListView(names);
                    itemList.setCellFactory(param -> new SchedulerCell());
                    
                    /*
                        Bind the width and height of the ListView to the given
                        bounds:
                    */
                    
                    itemList.prefWidthProperty().bind(itemListPane.widthProperty());
                    itemList.prefHeightProperty().bind(itemListPane.heightProperty());
                    
                    // Add the ListView the the container pane:
                    
                    itemListPane.getChildren().add(itemList);
                }
                catch (FileNotFoundException exception)
                {
                    // If the given directory was invalid:
                    
                    System.out.println(exception.getMessage());
                }
            }
            else
            {
                // If the parameters were invalid:
                
                System.out.println(validationMessage);
            }
		}
	}
	
    /**
        Used to initialize the FXML application:
        
        @param  url :   URL.
        @param  rb  :   ResourceBundle.
    **/
    
	@Override
	
	public void initialize(URL url, ResourceBundle rb)
	{
		
	}	
	
}
