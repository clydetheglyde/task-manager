package scheduler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
    @author Samuel Allen
**/

public final class FileManager
{
    // Class fields:
    
	private String fileName;
    private String fileNotFound;
	private File file;
    
    // Alternate constructor:
    
	public FileManager(String inFileName) throws FileNotFoundException
	{
		try
		{
			setFile(inFileName);
		}
		catch (FileNotFoundException exception)
		{
			throw (exception);
		}
	}
	
    /**
        Sets file to use the given directory and validates it:
        
        @param  inFileName              :   The desired directory.
        @throws FileNotFoundException
    **/
    
	public void setFile(String inFileName) throws FileNotFoundException
	{
        // Initialize the new file and file name:
        
		fileName = inFileName;
		file = new File(fileName);
        
        fileNotFound = ("There was no Readable File at the Given Directory: " +
                        fileName + ".");
		
        // Validate the directory:
        
		if (!file.exists())
		{
			throw new FileNotFoundException(fileNotFound);
		}
	}
	
    /**
        Reads the file and returns a map of list cell information:
        
        @return                         :   Map, the list cell information.
        @throws FileNotFoundException
    **/
    
	public Map<String, Boolean> read() throws FileNotFoundException
	{
        // Declare variables:
        
		Map<String, Boolean> items;
		Scanner scanner;
        Boolean cellCompleted;
		String line;
		String[] parts;
		
        // Initialize items and scanner as null:
        
		items = null;
		scanner = null;
		
        // Try to read the file:
        
		try
		{
            // Initialize the scanner and the map to store information:
            
			scanner = new Scanner(file);
			items = new HashMap<>();
            
            // Loop and read each line:
		
			while (scanner.hasNextLine())
			{
                // Read the next line and split it into parts:
                
				line = scanner.nextLine();
				parts = line.split(" : ");
                
                // If there is the correct amount of parts:

				if (parts.length == 2)
				{
                    // Get the boolean value from the string and validate it:
                    
                    cellCompleted = Boolean.parseBoolean(parts[1]);
                    
                    // If its valid add it to the information Map:
                    
                    if (cellCompleted != null)
                    {   
					    items.put(parts[0], cellCompleted);
                    }
                    else
                    {
                        // If the boolean value was invalid:
                        
                        // Log error:
                    }
				}
				else
				{
                    // If an invalid amount of parts was read:
                    
                    // Log error:
				}
			}
			
            // Close the scanner:
            
			scanner.close();
		}
		catch (FileNotFoundException exception)
		{
			// Try to close the scanner:
			
			if (scanner != null)
			{
				scanner.close();
			}
            
            // Throw an error:
            
			throw new FileNotFoundException(fileNotFound);
		}
		
		return (items);
	}
    
    /**
        Writes the given content to the file:
        
        @param  inContent   :   Map, the given Map of information.
        @throws IOException
    **/
	
	public void write(Map<String, Boolean> inContent) throws IOException
	{
        // Declare variables:
        
		String output;
		FileWriter fileOut;
		
        // Try to write to file:
        
		try
		{
            // Create new file writer and initialize output as empty:
            
			fileOut = new FileWriter(file, true);
			output = ("");
            
            /*
                Loop through the given map of content and add it to the output
                String:
            */
            
			for (Map.Entry<String, Boolean> entry : inContent.entrySet())
			{
				output += (entry.getKey() + " : " + entry.getValue() + "\n");
			}
            
            // Write the output to the file and close it:
            
			fileOut.write(output);
			fileOut.close();
		}
		catch (IOException exception)
		{
            // If the file could not be written to:
            
		    throw new IOException(fileNotFound);
		}
	}
}
